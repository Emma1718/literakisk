/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.1.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QFrame>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPlainTextEdit>
#include <QtWidgets/QProgressBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QTableWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QPushButton *connectBtn;
    QPushButton *quitBtn;
    QGroupBox *main;
    QPlainTextEdit *log;
    QLabel *label;
    QTableWidget *board;
    QFrame *frame;
    QPushButton *okBtn;
    QPushButton *sendBtn_2;
    QTableWidget *playerLetters;
    QLabel *label_6;
    QLabel *label_5;
    QLabel *label_7;
    QFrame *frame_2;
    QLabel *player1Name;
    QLabel *player2Name;
    QLabel *pntsPl1;
    QLabel *pntsPl2;
    QLabel *label_4;
    QLineEdit *nickEdit;
    QLabel *label_8;
    QLabel *label_9;
    QLineEdit *IPEdit;
    QLabel *label_10;
    QLineEdit *portEdit;
    QProgressBar *progressBar;

    void setupUi(QDialog *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->resize(865, 699);
        connectBtn = new QPushButton(MainWindow);
        connectBtn->setObjectName(QStringLiteral("connectBtn"));
        connectBtn->setGeometry(QRect(590, 10, 131, 31));
        quitBtn = new QPushButton(MainWindow);
        quitBtn->setObjectName(QStringLiteral("quitBtn"));
        quitBtn->setGeometry(QRect(740, 10, 121, 31));
        main = new QGroupBox(MainWindow);
        main->setObjectName(QStringLiteral("main"));
        main->setGeometry(QRect(20, 90, 841, 601));
        log = new QPlainTextEdit(main);
        log->setObjectName(QStringLiteral("log"));
        log->setEnabled(false);
        log->setGeometry(QRect(550, 410, 271, 171));
        label = new QLabel(main);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(550, 390, 50, 14));
        QFont font;
        font.setPointSize(8);
        label->setFont(font);
        board = new QTableWidget(main);
        if (board->columnCount() < 15)
            board->setColumnCount(15);
        QTableWidgetItem *__qtablewidgetitem = new QTableWidgetItem();
        board->setHorizontalHeaderItem(0, __qtablewidgetitem);
        QTableWidgetItem *__qtablewidgetitem1 = new QTableWidgetItem();
        board->setHorizontalHeaderItem(1, __qtablewidgetitem1);
        QTableWidgetItem *__qtablewidgetitem2 = new QTableWidgetItem();
        board->setHorizontalHeaderItem(2, __qtablewidgetitem2);
        QTableWidgetItem *__qtablewidgetitem3 = new QTableWidgetItem();
        board->setHorizontalHeaderItem(3, __qtablewidgetitem3);
        QTableWidgetItem *__qtablewidgetitem4 = new QTableWidgetItem();
        board->setHorizontalHeaderItem(4, __qtablewidgetitem4);
        QTableWidgetItem *__qtablewidgetitem5 = new QTableWidgetItem();
        board->setHorizontalHeaderItem(5, __qtablewidgetitem5);
        QTableWidgetItem *__qtablewidgetitem6 = new QTableWidgetItem();
        board->setHorizontalHeaderItem(6, __qtablewidgetitem6);
        QTableWidgetItem *__qtablewidgetitem7 = new QTableWidgetItem();
        board->setHorizontalHeaderItem(7, __qtablewidgetitem7);
        QTableWidgetItem *__qtablewidgetitem8 = new QTableWidgetItem();
        board->setHorizontalHeaderItem(8, __qtablewidgetitem8);
        QTableWidgetItem *__qtablewidgetitem9 = new QTableWidgetItem();
        board->setHorizontalHeaderItem(9, __qtablewidgetitem9);
        QTableWidgetItem *__qtablewidgetitem10 = new QTableWidgetItem();
        board->setHorizontalHeaderItem(10, __qtablewidgetitem10);
        QTableWidgetItem *__qtablewidgetitem11 = new QTableWidgetItem();
        board->setHorizontalHeaderItem(11, __qtablewidgetitem11);
        QTableWidgetItem *__qtablewidgetitem12 = new QTableWidgetItem();
        board->setHorizontalHeaderItem(12, __qtablewidgetitem12);
        QTableWidgetItem *__qtablewidgetitem13 = new QTableWidgetItem();
        board->setHorizontalHeaderItem(13, __qtablewidgetitem13);
        QTableWidgetItem *__qtablewidgetitem14 = new QTableWidgetItem();
        board->setHorizontalHeaderItem(14, __qtablewidgetitem14);
        if (board->rowCount() < 15)
            board->setRowCount(15);
        QTableWidgetItem *__qtablewidgetitem15 = new QTableWidgetItem();
        board->setVerticalHeaderItem(0, __qtablewidgetitem15);
        QTableWidgetItem *__qtablewidgetitem16 = new QTableWidgetItem();
        board->setVerticalHeaderItem(1, __qtablewidgetitem16);
        QTableWidgetItem *__qtablewidgetitem17 = new QTableWidgetItem();
        board->setVerticalHeaderItem(2, __qtablewidgetitem17);
        QTableWidgetItem *__qtablewidgetitem18 = new QTableWidgetItem();
        board->setVerticalHeaderItem(3, __qtablewidgetitem18);
        QTableWidgetItem *__qtablewidgetitem19 = new QTableWidgetItem();
        board->setVerticalHeaderItem(4, __qtablewidgetitem19);
        QTableWidgetItem *__qtablewidgetitem20 = new QTableWidgetItem();
        board->setVerticalHeaderItem(5, __qtablewidgetitem20);
        QTableWidgetItem *__qtablewidgetitem21 = new QTableWidgetItem();
        board->setVerticalHeaderItem(6, __qtablewidgetitem21);
        QTableWidgetItem *__qtablewidgetitem22 = new QTableWidgetItem();
        board->setVerticalHeaderItem(7, __qtablewidgetitem22);
        QTableWidgetItem *__qtablewidgetitem23 = new QTableWidgetItem();
        board->setVerticalHeaderItem(8, __qtablewidgetitem23);
        QTableWidgetItem *__qtablewidgetitem24 = new QTableWidgetItem();
        board->setVerticalHeaderItem(9, __qtablewidgetitem24);
        QTableWidgetItem *__qtablewidgetitem25 = new QTableWidgetItem();
        board->setVerticalHeaderItem(10, __qtablewidgetitem25);
        QTableWidgetItem *__qtablewidgetitem26 = new QTableWidgetItem();
        board->setVerticalHeaderItem(11, __qtablewidgetitem26);
        QTableWidgetItem *__qtablewidgetitem27 = new QTableWidgetItem();
        board->setVerticalHeaderItem(12, __qtablewidgetitem27);
        QTableWidgetItem *__qtablewidgetitem28 = new QTableWidgetItem();
        board->setVerticalHeaderItem(13, __qtablewidgetitem28);
        QTableWidgetItem *__qtablewidgetitem29 = new QTableWidgetItem();
        board->setVerticalHeaderItem(14, __qtablewidgetitem29);
        QTableWidgetItem *__qtablewidgetitem30 = new QTableWidgetItem();
        __qtablewidgetitem30->setTextAlignment(Qt::AlignHCenter|Qt::AlignVCenter|Qt::AlignCenter);
        board->setItem(0, 0, __qtablewidgetitem30);
        QTableWidgetItem *__qtablewidgetitem31 = new QTableWidgetItem();
        __qtablewidgetitem31->setTextAlignment(Qt::AlignHCenter|Qt::AlignVCenter|Qt::AlignCenter);
        board->setItem(0, 1, __qtablewidgetitem31);
        board->setObjectName(QStringLiteral("board"));
        board->setGeometry(QRect(10, 50, 531, 531));
        board->setMinimumSize(QSize(525, 525));
        board->setMaximumSize(QSize(540, 540));
        board->setAutoScroll(false);
        board->setAutoScrollMargin(0);
        board->horizontalHeader()->setVisible(false);
        board->horizontalHeader()->setDefaultSectionSize(35);
        board->verticalHeader()->setVisible(false);
        board->verticalHeader()->setDefaultSectionSize(35);
        board->verticalHeader()->setMinimumSectionSize(26);
        frame = new QFrame(main);
        frame->setObjectName(QStringLiteral("frame"));
        frame->setGeometry(QRect(550, 190, 271, 171));
        frame->setFrameShape(QFrame::StyledPanel);
        frame->setFrameShadow(QFrame::Raised);
        okBtn = new QPushButton(frame);
        okBtn->setObjectName(QStringLiteral("okBtn"));
        okBtn->setGeometry(QRect(30, 120, 91, 31));
        okBtn->setFont(font);
        sendBtn_2 = new QPushButton(frame);
        sendBtn_2->setObjectName(QStringLiteral("sendBtn_2"));
        sendBtn_2->setGeometry(QRect(140, 120, 91, 31));
        sendBtn_2->setFont(font);
        playerLetters = new QTableWidget(frame);
        if (playerLetters->columnCount() < 7)
            playerLetters->setColumnCount(7);
        if (playerLetters->rowCount() < 1)
            playerLetters->setRowCount(1);
        playerLetters->setObjectName(QStringLiteral("playerLetters"));
        playerLetters->setGeometry(QRect(10, 70, 251, 35));
        playerLetters->setMinimumSize(QSize(245, 35));
        playerLetters->setMaximumSize(QSize(300, 35));
        playerLetters->setRowCount(1);
        playerLetters->setColumnCount(7);
        playerLetters->horizontalHeader()->setVisible(false);
        playerLetters->horizontalHeader()->setDefaultSectionSize(35);
        playerLetters->verticalHeader()->setVisible(false);
        playerLetters->verticalHeader()->setDefaultSectionSize(35);
        label_6 = new QLabel(frame);
        label_6->setObjectName(QStringLiteral("label_6"));
        label_6->setGeometry(QRect(10, 40, 121, 20));
        label_6->setFont(font);
        label_5 = new QLabel(frame);
        label_5->setObjectName(QStringLiteral("label_5"));
        label_5->setGeometry(QRect(10, 10, 131, 29));
        label_5->setFont(font);
        label_7 = new QLabel(frame);
        label_7->setObjectName(QStringLiteral("label_7"));
        label_7->setGeometry(QRect(150, 10, 76, 29));
        label_7->setFont(font);
        frame_2 = new QFrame(main);
        frame_2->setObjectName(QStringLiteral("frame_2"));
        frame_2->setGeometry(QRect(550, 80, 271, 80));
        frame_2->setFrameShape(QFrame::StyledPanel);
        frame_2->setFrameShadow(QFrame::Raised);
        player1Name = new QLabel(frame_2);
        player1Name->setObjectName(QStringLiteral("player1Name"));
        player1Name->setGeometry(QRect(20, 10, 121, 16));
        QFont font1;
        font1.setPointSize(9);
        player1Name->setFont(font1);
        player2Name = new QLabel(frame_2);
        player2Name->setObjectName(QStringLiteral("player2Name"));
        player2Name->setGeometry(QRect(150, 10, 121, 16));
        player2Name->setFont(font1);
        pntsPl1 = new QLabel(frame_2);
        pntsPl1->setObjectName(QStringLiteral("pntsPl1"));
        pntsPl1->setGeometry(QRect(20, 30, 31, 29));
        pntsPl1->setFont(font1);
        pntsPl2 = new QLabel(frame_2);
        pntsPl2->setObjectName(QStringLiteral("pntsPl2"));
        pntsPl2->setGeometry(QRect(150, 30, 31, 29));
        pntsPl2->setFont(font1);
        label_4 = new QLabel(main);
        label_4->setObjectName(QStringLiteral("label_4"));
        label_4->setGeometry(QRect(550, 50, 171, 29));
        label_4->setMaximumSize(QSize(200, 16777215));
        label_4->setFont(font1);
        log->raise();
        label->raise();
        board->raise();
        frame->raise();
        frame_2->raise();
        label_4->raise();
        nickEdit = new QLineEdit(MainWindow);
        nickEdit->setObjectName(QStringLiteral("nickEdit"));
        nickEdit->setGeometry(QRect(370, 10, 171, 31));
        nickEdit->setMaxLength(12);
        label_8 = new QLabel(MainWindow);
        label_8->setObjectName(QStringLiteral("label_8"));
        label_8->setGeometry(QRect(320, 20, 81, 16));
        label_9 = new QLabel(MainWindow);
        label_9->setObjectName(QStringLiteral("label_9"));
        label_9->setGeometry(QRect(30, 20, 91, 16));
        IPEdit = new QLineEdit(MainWindow);
        IPEdit->setObjectName(QStringLiteral("IPEdit"));
        IPEdit->setGeometry(QRect(130, 10, 171, 31));
        label_10 = new QLabel(MainWindow);
        label_10->setObjectName(QStringLiteral("label_10"));
        label_10->setGeometry(QRect(30, 60, 91, 16));
        portEdit = new QLineEdit(MainWindow);
        portEdit->setObjectName(QStringLiteral("portEdit"));
        portEdit->setGeometry(QRect(130, 50, 171, 31));
        progressBar = new QProgressBar(MainWindow);
        progressBar->setObjectName(QStringLiteral("progressBar"));
        progressBar->setGeometry(QRect(320, 60, 541, 21));
        progressBar->setMinimumSize(QSize(4, 0));
        progressBar->setValue(0);
        progressBar->setTextVisible(true);
        progressBar->setTextDirection(QProgressBar::TopToBottom);

        retranslateUi(MainWindow);
        QObject::connect(quitBtn, SIGNAL(clicked()), MainWindow, SLOT(close()));

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QDialog *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "MainWindow", 0));
        connectBtn->setText(QApplication::translate("MainWindow", "Graj!", 0));
        quitBtn->setText(QApplication::translate("MainWindow", "Zako\305\204cz", 0));
        main->setTitle(QString());
        label->setText(QApplication::translate("MainWindow", "LOG", 0));
        QTableWidgetItem *___qtablewidgetitem = board->horizontalHeaderItem(0);
        ___qtablewidgetitem->setText(QApplication::translate("MainWindow", "1", 0));
        QTableWidgetItem *___qtablewidgetitem1 = board->horizontalHeaderItem(1);
        ___qtablewidgetitem1->setText(QApplication::translate("MainWindow", "2", 0));
        QTableWidgetItem *___qtablewidgetitem2 = board->horizontalHeaderItem(2);
        ___qtablewidgetitem2->setText(QApplication::translate("MainWindow", "3", 0));
        QTableWidgetItem *___qtablewidgetitem3 = board->horizontalHeaderItem(3);
        ___qtablewidgetitem3->setText(QApplication::translate("MainWindow", "4", 0));
        QTableWidgetItem *___qtablewidgetitem4 = board->horizontalHeaderItem(4);
        ___qtablewidgetitem4->setText(QApplication::translate("MainWindow", "5", 0));
        QTableWidgetItem *___qtablewidgetitem5 = board->horizontalHeaderItem(5);
        ___qtablewidgetitem5->setText(QApplication::translate("MainWindow", "6", 0));
        QTableWidgetItem *___qtablewidgetitem6 = board->horizontalHeaderItem(6);
        ___qtablewidgetitem6->setText(QApplication::translate("MainWindow", "7", 0));
        QTableWidgetItem *___qtablewidgetitem7 = board->horizontalHeaderItem(7);
        ___qtablewidgetitem7->setText(QApplication::translate("MainWindow", "8", 0));
        QTableWidgetItem *___qtablewidgetitem8 = board->horizontalHeaderItem(8);
        ___qtablewidgetitem8->setText(QApplication::translate("MainWindow", "9", 0));
        QTableWidgetItem *___qtablewidgetitem9 = board->horizontalHeaderItem(9);
        ___qtablewidgetitem9->setText(QApplication::translate("MainWindow", "10", 0));
        QTableWidgetItem *___qtablewidgetitem10 = board->horizontalHeaderItem(10);
        ___qtablewidgetitem10->setText(QApplication::translate("MainWindow", "11", 0));
        QTableWidgetItem *___qtablewidgetitem11 = board->horizontalHeaderItem(11);
        ___qtablewidgetitem11->setText(QApplication::translate("MainWindow", "12", 0));
        QTableWidgetItem *___qtablewidgetitem12 = board->horizontalHeaderItem(12);
        ___qtablewidgetitem12->setText(QApplication::translate("MainWindow", "13", 0));
        QTableWidgetItem *___qtablewidgetitem13 = board->horizontalHeaderItem(13);
        ___qtablewidgetitem13->setText(QApplication::translate("MainWindow", "14", 0));
        QTableWidgetItem *___qtablewidgetitem14 = board->horizontalHeaderItem(14);
        ___qtablewidgetitem14->setText(QApplication::translate("MainWindow", "15", 0));
        QTableWidgetItem *___qtablewidgetitem15 = board->verticalHeaderItem(0);
        ___qtablewidgetitem15->setText(QApplication::translate("MainWindow", "1", 0));
        QTableWidgetItem *___qtablewidgetitem16 = board->verticalHeaderItem(1);
        ___qtablewidgetitem16->setText(QApplication::translate("MainWindow", "2", 0));
        QTableWidgetItem *___qtablewidgetitem17 = board->verticalHeaderItem(2);
        ___qtablewidgetitem17->setText(QApplication::translate("MainWindow", "3", 0));
        QTableWidgetItem *___qtablewidgetitem18 = board->verticalHeaderItem(3);
        ___qtablewidgetitem18->setText(QApplication::translate("MainWindow", "4", 0));
        QTableWidgetItem *___qtablewidgetitem19 = board->verticalHeaderItem(4);
        ___qtablewidgetitem19->setText(QApplication::translate("MainWindow", "5", 0));
        QTableWidgetItem *___qtablewidgetitem20 = board->verticalHeaderItem(5);
        ___qtablewidgetitem20->setText(QApplication::translate("MainWindow", "6", 0));
        QTableWidgetItem *___qtablewidgetitem21 = board->verticalHeaderItem(6);
        ___qtablewidgetitem21->setText(QApplication::translate("MainWindow", "7", 0));
        QTableWidgetItem *___qtablewidgetitem22 = board->verticalHeaderItem(7);
        ___qtablewidgetitem22->setText(QApplication::translate("MainWindow", "8", 0));
        QTableWidgetItem *___qtablewidgetitem23 = board->verticalHeaderItem(8);
        ___qtablewidgetitem23->setText(QApplication::translate("MainWindow", "9", 0));
        QTableWidgetItem *___qtablewidgetitem24 = board->verticalHeaderItem(9);
        ___qtablewidgetitem24->setText(QApplication::translate("MainWindow", "10", 0));
        QTableWidgetItem *___qtablewidgetitem25 = board->verticalHeaderItem(10);
        ___qtablewidgetitem25->setText(QApplication::translate("MainWindow", "11", 0));
        QTableWidgetItem *___qtablewidgetitem26 = board->verticalHeaderItem(11);
        ___qtablewidgetitem26->setText(QApplication::translate("MainWindow", "12", 0));
        QTableWidgetItem *___qtablewidgetitem27 = board->verticalHeaderItem(12);
        ___qtablewidgetitem27->setText(QApplication::translate("MainWindow", "13", 0));
        QTableWidgetItem *___qtablewidgetitem28 = board->verticalHeaderItem(13);
        ___qtablewidgetitem28->setText(QApplication::translate("MainWindow", "14", 0));
        QTableWidgetItem *___qtablewidgetitem29 = board->verticalHeaderItem(14);
        ___qtablewidgetitem29->setText(QApplication::translate("MainWindow", "15", 0));

        const bool __sortingEnabled = board->isSortingEnabled();
        board->setSortingEnabled(false);
        QTableWidgetItem *___qtablewidgetitem30 = board->item(0, 0);
        ___qtablewidgetitem30->setText(QApplication::translate("MainWindow", "a", 0));
        board->setSortingEnabled(__sortingEnabled);

        okBtn->setText(QApplication::translate("MainWindow", "OK", 0));
        sendBtn_2->setText(QApplication::translate("MainWindow", "PASUJ", 0));
        label_6->setText(QApplication::translate("MainWindow", "TWOJE LITERY", 0));
        label_5->setText(QApplication::translate("MainWindow", "POZOSTA\305\201Y CZAS:", 0));
        label_7->setText(QApplication::translate("MainWindow", "60", 0));
        player1Name->setText(QString());
        player2Name->setText(QString());
        pntsPl1->setText(QApplication::translate("MainWindow", "0", 0));
        pntsPl2->setText(QApplication::translate("MainWindow", "0", 0));
        label_4->setText(QApplication::translate("MainWindow", "LICZBA PUNKT\303\223W", 0));
        label_8->setText(QApplication::translate("MainWindow", "Nick", 0));
        label_9->setText(QApplication::translate("MainWindow", "IP serwera", 0));
        label_10->setText(QApplication::translate("MainWindow", "Port", 0));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
