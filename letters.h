#ifndef LETTERS_H
#define LETTERS_H

#include <map>
#include <string.h>
#include <iostream>
#include <fstream>
#include <string>

struct letter {
  std::string c;
  int val;
};

struct sack {
  std::map<int, letter> letters;
  int size;
  std::map<std::string, int> valsOfLetters;
};

void readLetters(sack*s, char * arg);
std::string getLetters(sack *s, int amount);
std::string createPocketLetVals(sack s);

#endif
