#-------------------------------------------------
#
# Project created by QtCreator 2014-01-18T21:20:06
#
#-------------------------------------------------
DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x000000
QT       += core gui network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Literaki
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    Map.cpp

HEADERS  += mainwindow.h \
    Map.h

FORMS    += mainwindow.ui
