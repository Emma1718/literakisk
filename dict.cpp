#include "dict.h"


using namespace std;

bool checkWords(list<string> wordsToCheck, set<string> words) {

  set<string>::iterator iter;
  for(list<string>::iterator i = wordsToCheck.begin(); i != wordsToCheck.end(); i++) {
    iter = words.find(*i);

    if (iter==words.end ())
      return false;
   
  }
  return true;
 
}

void readDict(string filename, set<string> *words) {

  ifstream read_file(filename.c_str(), ifstream::in );
  string s;

  if(read_file.good() == false)
    cout << "couldn't open file containing dictionary!" << endl;
  
  while (read_file>>s)
  {
      words->insert(s);
    }
  read_file.close();
}
