#ifndef MAP_H
#define MAP_H

#define ROW_COUNT 15
#define COL_COUNT 15

class Map {




public:
    explicit Map();

    bool checkMove(int &cs, int &mods);
    bool checkIfSet(int x, int y);
    void modify(int i, int j, bool x);
    int checkRow(int x );
    int checkCol(int y );
    void setModifiedFields();
    void setField(int i, int j);
    bool checkIfModified(int i, int j);
    void deleteMods();

private:
    void init_map();
    bool modified[ROW_COUNT][COL_COUNT];
    bool set[ROW_COUNT][COL_COUNT];


};




#endif // MAP_H
