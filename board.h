#ifndef MAP_H
#define MAP_H

#include <sys/socket.h>
#include <netinet/in.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <strings.h>
#include <unistd.h>
#include <pthread.h>
#include <string.h>
#include <iostream>
#include <fstream>
#include <list>
#include "letters.h"

#define COL_NUM 15
#define ROW_NUM 15

using namespace std; 
struct board {
  int matrix[COL_NUM][ROW_NUM];
  letter let_board[COL_NUM][ROW_NUM];
  letter tmp_board[COL_NUM][ROW_NUM];
  int startMatrix[COL_NUM][ROW_NUM];
};



void loadFromFile(board * m, char* arg);
void createPocket(char * buf, board* m);
void initLettersBoard(letter letB[ROW_NUM][COL_NUM]);
void cloneBoard(letter boardTo[ROW_NUM][COL_NUM], letter boardFrom[ROW_NUM][COL_NUM]);
void findWords(list <string> *words, int opt, board b,  list<int> mods, int line, int *points);
int goRight(int, int, letter b[ROW_NUM][COL_NUM]);
int goLeft(int, int, letter b[ROW_NUM][COL_NUM]);
int goDown(int, int, letter b[ROW_NUM][COL_NUM]);
int goUp(int, int, letter b[ROW_NUM][COL_NUM]);
int countPoints(int opt, int begin, int end, int x, board b);
void clearBoards(board * b);
void cloneMatrix(int mTo[ROW_NUM][COL_NUM], int mFrom[ROW_NUM][COL_NUM]);

#endif
