#include <cstdlib>
#include <ctime>
#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <sstream>
#include "letters.h"


using namespace std;

void readLetters(sack * s, char * arg) 
{
  ifstream read_file(arg, ifstream::in);

  int amount;

  if(read_file.good() ==false)
    {
      cout << "Couldn't read file " << arg << endl;
    }

  for(int x=0; 1;)
    {
      letter l;
      string c;
      int v;
      read_file>>c>>amount>>v;
      l.c = c;
      l.val = v;
      if(read_file.good())
        {
          for(int i=0; i<amount; i++)
            {
	      s->letters.insert(pair<int,letter>(x,l));
              s->valsOfLetters.insert(pair<string, int>(l.c, l.val));
	      x++;
            }
        }
      else
        break;
    }
  s->size = s->letters.size();
  read_file.close();

}

string getLetters(sack *s, int amount)
{

  srand(time(NULL));
  
  string lettersBuf =  "";

  int size = s->letters.size();

  map<int,letter>::iterator iter;
 
  while(amount>0 && size>0)
    {
      srand(time(NULL));      
      int r = rand()%(s->size);
      iter=s->letters.find(r);
 

     while(iter == s->letters.end())
        {
          r = rand()%(s->size);
          iter = s->letters.find(r);
        }


      lettersBuf = lettersBuf +  (*iter).second.c;

      if(iter==s->letters.begin())
        {
          s->letters.erase(iter);
          s->letters.begin()= iter++;
        }
      else
        {
          if(iter==s->letters.end())
            s->letters.erase(iter,s->letters.end());
          else
            s->letters.erase(iter);
        }
      --size;
      --amount;
    }

  cout << "Randomized letters: " << lettersBuf << endl;
  return lettersBuf;
}

string createPocketLetVals(sack s) {

  string lettersBuffer = "";
  
  for(map<string,int>::iterator it = s.valsOfLetters.begin(); it !=  s.valsOfLetters.end(); it++) {
    lettersBuffer += (*it).first;
    std::ostringstream ss;
    ss <<  (*it).second;
    string str = ss.str();      
    lettersBuffer += str;
  }

  return lettersBuffer;
}  
