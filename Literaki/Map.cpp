#include <iostream>
#include "Map.h"


using namespace std;

void Map::setModifiedFields() {
    for(int i = 0; i < ROW_COUNT; i++)
        for(int j = 0; j < COL_COUNT; j++)
            if(modified[i][j]) {
                set[i][j] = true;
            }
}

void Map::deleteMods() {
    for(int i = 0; i < ROW_COUNT; i++)
        for(int j = 0; j < COL_COUNT; j++)
            if(modified[i][j]) {
                modified[i][j] = false;
            }
}

void Map::setField(int i, int j) {
    set[i][j] = true;
}

bool Map::checkIfModified(int i, int j) {
    return modified[i][j];

}
void Map::modify(int i, int j, bool x)
{
    modified[i][j] = x;
}
bool Map::checkIfSet(int x, int y) //sprawdz czy dane pole jest ustawione, tzn. czy posiada literę i nie jest modyfikowane
{

    if((x>=0) && (y>=0) && (x<ROW_COUNT) && (y<COL_COUNT))
    {
        if ((set[x][y]))
            return true;
        else
            return false;
    }
    else return false;
}

bool Map::checkMove(int &cs, int &mods) {

    int mod_amount = 0, x=-1, y=-1;

    for( int i = 0; i < ROW_COUNT; i++) //zlicz wszsytkie modyfikacje
        for( int j = 0; j < COL_COUNT; j++)
        {
            if(this->modified[i][j])
            {
                    if (mod_amount==0)
                {
                    x = i; //pobierz współrzędne pierwszego zmodyfikowanego
                    y = j;
                }
                mod_amount++;
            }
        }
    if (x<0) return false;

    cs = 0;
    mods = 0;

    if (this->checkRow(x) != mod_amount) //jesli w wierszu nie ma wszsytkich zmodyfikowanych pól
    {
        if ((this->checkCol(y)) != mod_amount) return false; //oraz w kolumnie, niepoprawny ruch
        cs = 2; //jesli jest w kolumnie, przekaz informacje 2
    }
    if (cs == 0) cs = 1; //jesli jest w wierszu przekaz informacje 1

    /*---------Gdy nie zreturnuje to znaczy ze albo w kolumnie albo w wierszu znajduja sie wszystkie zmodyfikowane przyciski */

    for(int i = 0; i < ROW_COUNT; i++)
        for(int j = 0; j < COL_COUNT; j++)
            if (this->modified[i][j])
            {
                if (((i==((COL_COUNT-1)/2)) && (i==j)) || this->checkIfSet(i+1, j) || this->checkIfSet(i-1, j) || this->checkIfSet(i, j+1) || this->checkIfSet(i, j-1))
                {
                    mods = mod_amount;
                    return true; // jeśli choć jedno pole ma w otoczeniu ustawione litery bądź znajduje się pośrodku planszy - poprawnych ruch
                }
            }
    return false; //jeśli wyraz nie graniczy z innym niepoprawny ruch

}


int Map::checkRow(int x)
{
    int actual_amount = 0;
    bool interested = false;

    for(int j = 0; j < COL_COUNT; j++)
    {
        if (interested)
        {
            if (!set[x][j] && !modified[x][j])
                break;
            else if (this->modified[x][j])
                actual_amount++;
        }
        else if (this->modified[x][j])
        {
            actual_amount++;

            interested = true;
        }
    }

    return actual_amount;

}

int Map::checkCol(int y)
{
    int actual_amount = 0;
    bool interested = false;

    for(int j = 0; j < ROW_COUNT; j++)
    {
        if (interested)
        {
            if (!set[j][y] && !modified[j][y])
                break;
            else if (this->modified[j][y])
                actual_amount++;
        }
        else if (this->modified[j][y])
        {
            actual_amount++;
            interested = true;
        }
    }
    return actual_amount;
}

Map::Map() {
    init_map();
}

void Map::init_map() {
    for(int i = 0; i<ROW_COUNT; i++)
        for(int j = 0; j<COL_COUNT; j++)
        {
            modified[i][j] = false;
            set[i][j] = false;
        }
}
