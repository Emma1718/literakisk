#include "board.h"
#include "letters.h"
#include "dict.h"
#include <string>
#include <sstream>
#include <list>

#define CON_LIMIT 2

using namespace std;


ushort service_port = 12345;
pthread_mutex_t sock_mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_t client_threads[CON_LIMIT];
pthread_t main_thread;
int cln_sck[CON_LIMIT];

char players[2][30];
int points[2];

board m;
sack s;
set <string> dictionary;
int giveUps = 0;

void* client_loop(void* arg)
{
  char buffer[1024]; //do odbierania wiadomości z gniazdka
  int sck = *((int*) arg);
  int rcv;
  int num = 0;

  pthread_mutex_lock(&sock_mutex);
  for (int i = 0; i < CON_LIMIT; i++){
    if (cln_sck [i] == sck) {
      num = i;
      break;
    }
  }
  printf("Socket %d\n", sck);
  pthread_mutex_unlock(&sock_mutex);

  char *board, *bufToSend;// = "B|";      
  board = new char[225];
  bufToSend = new char[228];
  strcpy(bufToSend, "B|");
  createPocket(board, &m); //plansza w postaci łancucha znakowego
  strcat(bufToSend, board);
  strcat(bufToSend, "#");
  write(sck, bufToSend, strlen(bufToSend));//wysłanie wiadomości o planszy "B|plansza#"
  
  delete [] board;
  delete [] bufToSend;
  //TO JEST DODANE. TUTAJ BLEDU NIE MA ALE POTEM JEST !!!!!
  string letVals = createPocketLetVals(s);
  char * valsOfLets = new char[80];
  strcpy(valsOfLets, "L|");
  strcat(valsOfLets, letVals.c_str());
  strcat(valsOfLets, "#");
  
  // if(write(sck, valsOfLets, strlen(valsOfLets))) //wyśli wszsytkie litery wraz z wartościami
  //   cout << valsOfLets << endl;
  delete [] valsOfLets;   
  
  while((rcv=read(sck, buffer, 1024))>0)
    {
      if (buffer[0] == 'S') //odebranie nazwy użytkownika
	{	
	  char * name = strtok(buffer, "|");
	  name = strtok(NULL, "|");
	  //pthread_mutex_lock(&sock_mutex);
	  strcpy(players[num], name);
	  printf("Hi, %s\n", players[num]);
	  while(strcmp(players[1-num],"")== 0); //poczekaj aż przybędzie rywal 
	  char buf[20];
	  strcpy(buf, "O|");
	  strcat(buf, players[1-num]);
	  strcat(buf, "#");
	  write(sck, buf, strlen(buf)); //wyślij  
	  
	}
      //pthread_mutex_unlock(&sock_mutex);    
      if (buffer[0] == 'G') //prośba o litery 
	{	

	  char * amount = strtok(buffer, "|"); //odczytaj ile liter gracz chce
	  amount = strtok(NULL, "|");
	  int num = atoi(amount);
	
	  string letters = getLetters(&s,num); //pobierz litery dla gracza
	  char * letToSend1 = new char[num+3];
	  strcpy(letToSend1, "G|");
	  strcat(letToSend1, letters.c_str());
	  strcat(letToSend1, "#");

	  // if(write(sck, letToSend1, strlen(letToSend1))) //wyślij wylosowane litery
	  //   cout << "Letters "<< letToSend1 << endl;	  
	  delete [] letToSend1;
	  letters.clear();
	} 
      if (buffer[0] == 'C') //zgłoszenie ruchu - przesłanie liter wraz z ich współrzędnymi na planszy| C - coordinates
	{	
	  int mod_amount = buffer[2]-'0'; //liczba modyfikacji
	  int rowOrCol = buffer[1]-'0'; //info o tym czy modyfikacje w kolumnie czy wierszu
	  if(mod_amount == 0) {
	    cout << "Player " << num+1 << " give up in this move" << endl;
	    cout << "Turn: PLAYER " << 1-num << endl;
	    write(cln_sck[1-num], "M|0#", 4);
	    giveUps++;
	    if(giveUps == 4) {
	      cout << "End of Game - two players gave up twice" << endl;
	      write(sck, "E|letters#", 9);
	      write(cln_sck[1-num], "E|letters#", 9);
	    }
	  } else {
	
	    
	    char * coords = strtok(buffer, "|"); //format wiadomości: C|litera.x.y!....#
	    coords = strtok(NULL, "|"); 
	    //przygotowanie wiadomości dla przeciwnika
	    char* msgToOpp = new char[strlen(coords)+3];
	    strcpy(msgToOpp, "M|");
	    strcat(msgToOpp, coords);

	    list<string> threes;
	    list<string>::iterator iter;
	    list<string>wordsToCheck;
	    list<int> mods;
	    int line;
	  
	    cloneBoard(m.tmp_board, m.let_board);
	 
	    string three;
	    three = strtok (coords,"!"); //podzielenie względem !
	    while (three.c_str() != NULL && three.compare("#") != 0)
	      {
		threes.push_back(three);   
		three = strtok (NULL, "!");	      
	      }
	    //lista "trójek" - litera, wsp.x, wsp.y
	    for(iter=threes.begin(); iter != threes.end(); iter++)
	      {
		//cout<<*iter<<endl;
		int counter = 0;
		char * pch = strdup((*iter).c_str());
		pch  = strtok(pch, ".");
		int x, y;
		letter let;
		//sprawdzanie gdzie wstawić daną literę w tymczasowej planszy
		while(pch != NULL)
		  {
		    if (counter==0) {
		      let.c = pch;
		      let.val = s.valsOfLetters.find(let.c)->second;
		      //cout << let.val << endl;
		    }		

		    if (counter==1)
		      x = atoi(pch);
		
		    if (counter==2)
		      y = atoi(pch);
		  
		    pch = strtok(NULL, ".");
		 
		    counter++;
		  }
		cout << let.c << " on: " << x << " " << y << endl;
		m.tmp_board[x][y] = let;
		if(rowOrCol==1) {
		  line = x;
		  mods.push_back(y); //wspolrzędne modyfikacji
		}
		else {
		  mods.push_back(x);
		  line = y;
		}

	      }
	    int predictedSum = 0; //przewidywana suma punktów - dodanie gdy wyrazy poprawne
	    findWords(&wordsToCheck, rowOrCol, m, mods, line, &predictedSum); //sprawdzenie jakie wyrazy zostały znalezione, sprawdzenie przewidywanych punktów
	    for (list<string>::iterator it=wordsToCheck.begin(); it != wordsToCheck.end(); ++it)
	      cout << "word before checking: " << (*it) << endl;	
	   
	    if( checkWords(wordsToCheck, dictionary)) { //jeśli wszystkie wyrazy znalezione
	      giveUps = 0;
	      cloneBoard(m.let_board, m.tmp_board); //przepisz tymczasową planszę do głównej
	      //usunięcie bonusów
	      if(rowOrCol==1) {
		for (list<int>::iterator it=mods.begin(); it != mods.end(); ++it){ 
		  m.matrix[line][*it] = 0;
		}
	      }
	      else {
		for (list<int>::iterator it=mods.begin(); it != mods.end(); ++it){
		  m.matrix[*it][line] = 0;
		}	      
	      }

	      write(sck, "C|correct#", 10); //prześlij info do gracza o poprawnym ruchu
	      //pinkty - wysłanie do obu graczy informacji o puntkach
	      points[num]+=predictedSum;	    
	      ostringstream ss1 ,ss2;
	      ss1 << points[num];
	      ss2 << points[1-num];
	      string infoAboutP1 = "P|" + ss1.str() + "&"  + ss2.str() + "#";
	      string infoAboutP2 = "P|" + ss2.str() + "&"  + ss1.str() + "#";
	  
	      char * msg1 = new char[infoAboutP1.length()];
	      char * msg2 = new char[infoAboutP2.length()];
	      strcpy(msg1, infoAboutP1.c_str());
	      strcpy(msg2, infoAboutP2.c_str());
	  
	      write(sck, msg1, strlen(msg1));
	      write(cln_sck[1-num], msg2, strlen(msg2));
	      //wysłanie informacji o wstawionych literach
	      write(cln_sck[1-num], msgToOpp, strlen(msgToOpp));
	  

	      delete [] msg1;
	      delete [] msg2;
	      delete [] msgToOpp;
	  
	      string newLetters = getLetters(&s,mod_amount);
	      char * newLToSend = new char[mod_amount+3];
	      strcpy(newLToSend, "G|");
	      strcat(newLToSend, newLetters.c_str());
	      strcat(newLToSend, "#");

	      // if(write(sck, newLToSend, strlen(newLToSend))) //wyślij wylosowane litery
	      // 	cout << "Letters: "<< newLToSend << endl;	  
	      delete [] newLToSend;
	      newLetters.clear();
	
	      //wypisanie planszy
	      for(int i = 0; i < ROW_NUM; i++) {
		for(int j = 0; j < COL_NUM; j++) {
		  if(m.let_board[i][j].c == "") 
		    cout << "_";
		  else 
		    cout << m.let_board[i][j].c;
		}
		cout << endl;
	      }
	    }
	    else {
	      cout << "Incorrect words" << endl;
	      giveUps++;
	      if(giveUps == 4) {
		cout << "End of Game - two players gave up twice" << endl;
		write(sck, "E|letters#", 9);
		write(cln_sck[1-num], "E|letters#", 9);
	      } else {
		write(sck, "C|incorrect#", 13); //prześlij info do gracza o niepoprawnym ruchu
		write(cln_sck[1-num], "M|0#", 4);
	      }
	    }
	  }
	}
      if(buffer[0] == 'E') {
	char * leftLetters = strtok(buffer, "|");
	leftLetters = strtok(NULL, "|");
	//cout << leftLetters << endl;
	leftLetters = strtok(leftLetters, ".");
	while(leftLetters != NULL) {
	  //cout << leftLetters << endl;	 
	  points[num]-=s.valsOfLetters.find(leftLetters)->second;
	  leftLetters = strtok(NULL, ".");	 
	}
	cout << "Player's "<< num << " points: "<< points[num]<<endl;
	ostringstream ss1 ,ss2;
	ss1 << points[num];
	ss2 << points[1-num];
	string infoAboutP1 = "P|" + ss1.str() + "&"  + ss2.str() + "#";
	string infoAboutP2 = "P|" + ss2.str() + "&"  + ss1.str() + "#";
	  
	char * msg1 = new char[infoAboutP1.length()];
	char * msg2 = new char[infoAboutP2.length()];
	strcpy(msg1, infoAboutP1.c_str());
	strcpy(msg2, infoAboutP2.c_str());
	  
	write(sck, msg1, strlen(msg1));
	write(cln_sck[1-num], msg2, strlen(msg2));
	  
	if(points[num] > points[1-num]) {
	  cout << "Player " << num << " is winner!" << endl;
	  write(sck, "E|winner#", 9);
	} else if (points[num] == points[1-num]) {
	  cout << "DRAW!" <<endl;
	  write(sck, "E|draw#", 7);
	} else 	  
	  write(sck, "E|looser#", 9);
	
      }
    }


  pthread_mutex_lock(&sock_mutex);
  cln_sck [num] = 0;//cln_sck[1];
  close(sck); 
  if(cln_sck[1-num] != 0) {
    write(cln_sck[1-num], "DIS", 4);
   
    strcpy(players[0], "");
    strcpy(players[1], "");
    points[0] = 0;
    points[1] = 0;

    s.valsOfLetters.clear();
    s.letters.clear();

    clearBoards(&m);

    char file2[16] =  "./letters.txt"; 
    readLetters(&s, file2);
    cloneMatrix(m.matrix, m.startMatrix);
    giveUps = 0;
  }
  pthread_mutex_unlock(&sock_mutex);
  
  printf("Socket %d is getting closed...\n", sck);
  return 0;

}


void* main_loop(void* arg)
{
  struct sockaddr_in server_addr, cln_addr;

  int sck, tmp_sck;

  int i = CON_LIMIT;

  bzero(&server_addr, sizeof(server_addr));
  server_addr.sin_addr.s_addr = INADDR_ANY;
  server_addr.sin_family = AF_INET;
  server_addr.sin_port = htons(service_port);
  int x;
  for(x = 0;x<CON_LIMIT;x++)
    {
      cln_sck[x] = 0;
    }

  if((sck = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0) 
    {
      perror("Nie można utworzyć gniazdka");
      exit(EXIT_FAILURE);
    }

  int opt=1;

  // lose the pesky "Address already in use" error message
  if (setsockopt(sck,SOL_SOCKET,SO_REUSEADDR,&opt,sizeof(int)) == -1) {
    perror("setsockopt");
    exit(1);
  }  
 

  if(bind(sck, (struct sockaddr*) &server_addr, sizeof server_addr) < 0)
    {
      printf("Cannot bind socket %d in %d port \n", sck, service_port);
      exit(EXIT_FAILURE);
    }

  if(listen(sck,-1) < 0) 
    {
      perror("Cannot listen"); 
      exit(EXIT_FAILURE);
    }

  printf("Main loop\n");
  while(1)
    {
     
      socklen_t cln_addr_size = sizeof cln_addr;
  
      if((tmp_sck = accept(sck, (struct sockaddr*) &cln_addr, (socklen_t*) &cln_addr_size)) <0)
	{
	  perror("Main socket accept error\n");
	  exit(EXIT_FAILURE);
	}
      
      
      pthread_mutex_lock(&sock_mutex);
      i = CON_LIMIT;
      int j;
      for(j = 0; j<CON_LIMIT; j++)
	{
	  if(cln_sck[j] == 0)
	    {
	      i = j;
	      cln_sck[j] = tmp_sck;
	      printf("Accepting socket...\n");
	      if(cln_sck[1-i] == 0) {
		printf("First player, waiting for opponent...");
		char * buffer =  new char[7];
		strcpy(buffer, "S|wait#"); 
		write(cln_sck[i], buffer, strlen(buffer));
	      } else {
		printf("Opponent is connected! Game is starting...");
		char * buf2 = new char[7];
		strcpy(buf2, "S|true#");
		write(cln_sck[1-i],buf2,strlen(buf2)); 
		char * buf1 = new char[7];
		strcpy(buf1, "S|false#");
		write(cln_sck[i],buf1,strlen(buf1));	
	      }	   
	      break;	    
	    }
	}

      //       sleep(1);
      
      pthread_mutex_unlock(&sock_mutex);
  

      if(i == CON_LIMIT)
	{
	  printf("Too many connections!\n");
	  close(tmp_sck);
	  continue;
	}

      if(pthread_create(&client_threads[i], NULL, client_loop, &cln_sck[i]) !=0)
	{
	  printf("Error creating new client thread");
	  continue;
	}
    }	
    
}

int main( int argc, char** argv)
{
  strcpy(players[0], "");
  strcpy(players[1], "");
  points[0] = 0;
  points[1] = 0;

  char file1[13] = "./matrix.txt";
  loadFromFile(&m, file1);
  cloneMatrix(m.matrix, m.startMatrix);

  char file2[16] =  "./letters.txt"; 
  readLetters(&s, file2);
  initLettersBoard(m.let_board);
  readDict("words.txt", &dictionary);
  if (pthread_create(&main_thread, NULL, main_loop, NULL) != 0) {
    printf("ERROR");

  }
  printf("Start\n");
  getchar();

  return 0;
}

