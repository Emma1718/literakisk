#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QDialog>
#include <QHostInfo>
#include <QTcpSocket>
#include <QMessageBox>
#include <QTimer>

#include <QMap>

#include "Map.h"

#define ROW_COUNT 15
#define COL_COUNT 15

namespace Ui {
class MainWindow;
}

class MainWindow : public QDialog
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    void new_game();
    void readBoard(char * c);
    void colorItem(int i, int j, int color);
    void showPlayerLetters(char * letters);
    void loadValsOfLetters(char * vals);
    void setLetters(QByteArray letters);
    void deleteModLetters();
    void endOfGame();
    void setEnabledForAll(bool x);
    void sendLeftLetters();
    ~MainWindow();

private:
    Ui::MainWindow *ui;
    QTcpSocket *socket;
    bool myTurn = false;
    QString tmp_letter;
    QColor tmp_color;
    QString myName;
    int matrix[ROW_COUNT][COL_COUNT];
    QMap<QString, int> valsOfLetters;


    bool flags[5];
    bool gameStart = false;
    int tmove;
    QTimer *timer;
    Map *map;

private slots:
//void on_sendBtn_clicked(); //polaczenie automatyczne
void myBoardCellClicked(int i, int j);
void myLettersCellClicked(int i, int j);
void on_okBtn_clicked();
void on_connectBtn_clicked();
void hostLookedUp(const QHostInfo &info); //
void socketError(QAbstractSocket::SocketError);
void socketConnected();
void socketDisconnected();
void readAnswer();
void timerEnd();
void on_passBtn_clicked();

};


#endif // MAINWINDOW_H
