#ifndef DICT_H
#define DICT_H

#include <set>
#include <list>
#include <iostream>
#include <fstream>

using namespace std;

//set<string> words;
bool checkWords(list <string> wordsToCheck, set<string> words);
void readDict(string filename, set<string> *words);



#endif
