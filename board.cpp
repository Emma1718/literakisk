#include "board.h"

using namespace std;

void loadFromFile(board* m, char* arg) 
{
  ifstream file(arg, ifstream::in);
  int x;

  if(file.good() == false)
    {
      string ex = "couldn't open file containing map!";
      throw ex;
    }

  if (file.is_open())
    for(int i=0;i<15;i++)
      {
	for(int j=0;j<15;j++)
	  {
	    file>>x;
	    m->startMatrix[i][j] = x;
	  }
      }
}

void createPocket(char *buffer, board* m) {
  //buffer = new char[226];
  memset( buffer, 0, sizeof( buffer ) );  
  
  for(int i=0;i<15;i++)
    {
      for(int j=0;j<15;j++)
	{
	  buffer[(j%15)+15*i] = m->startMatrix[i][j] + '0'; 
	}
    }
  //buffer[225] = '\0';  
 
}

void initLettersBoard(letter letB[ROW_NUM][COL_NUM]) {

  for(int i = 0; i < ROW_NUM; i++)
    for(int j = 0; j < COL_NUM; j++) {
      letB[i][j].c = "";
      letB[i][j].val = 0;
    }

}

void cloneBoard(letter boardTo[ROW_NUM][COL_NUM], letter boardFrom[ROW_NUM][COL_NUM] ) {

  for(int i = 0; i < ROW_NUM; i++)
    for(int j = 0; j < COL_NUM; j++) {
      boardTo[i][j].c = boardFrom[i][j].c;
      boardTo[i][j].val = boardFrom[i][j].val;
    }

}

void cloneMatrix(int mTo[ROW_NUM][COL_NUM], int mFrom[ROW_NUM][COL_NUM]) {
  for(int i = 0; i < ROW_NUM; i++)
    for(int j = 0; j < COL_NUM; j++) {
      mTo[i][j] = mFrom[i][j];
      mTo[i][j] = mFrom[i][j];
    }
}
void findWords(list <string> *words, int opt, board b, list<int> mods, int line, int *points)  //znajdz wyrazy i przekaż je za pomocą listy, pobierz informację czy zmodyfikowane pola są w wierszu czy kolumnie (
{
  bool found1 = false;
  int begin, end;
  string word;
  list<int>::iterator it;
  int sum = 0;

  for(it = mods.begin(); it != mods.end(); it++) {
    if (!found1) {
      found1 = true;
      switch(opt) {
      case 1: //modyfikacje w wierszu                                                                                                                         
	{
	  begin = goLeft(line,(*it)-1, b.tmp_board);
	  end = goRight(line, (*it)+1, b.tmp_board);
	  if (begin < end) {//jesli wyraz jest co najmniej 2literowy 
	    for(int p = begin; p<=end; p++) //znajdz go                          
	      word+=b.tmp_board[line][p].c;
	  words->push_back(word); //wrzuc na listę                              
	  word.clear();//i wyczyść                          
	  sum += countPoints(1, begin, end, line, b); 
	  }                                                                                              
	}
	break;
      case 2: //modyfikacje w kolumnie                                                                                                                        
	{ 
	  begin = goUp((*it)-1,line, b.tmp_board);
	  end = goDown((*it)+1, line, b.tmp_board);
	  if (begin < end)
	    {
	      for(int p = begin; p<=end; p++)
		word+=b.tmp_board[p][line].c;
	      words->push_back(word);
	      word.clear();
	      sum += countPoints(2, begin, end, line, b);
	    }	 
	}
	break;
      }
      it--;
    }
    else //jesli to juz następne zmodyfikowane pole                                                                                                               
      {
	switch(opt)
	  {
	  case 1: //to znajduj kolejne wyrazy w kolumnie                                                                                                          
	    {
	      begin = goUp(line-1,(*it), b.tmp_board);
	      end = goDown(line+1,(*it), b.tmp_board);
	      if (begin < end)
		{
		  for(int p = begin; p<=end; p++)
		    word+=b.tmp_board[p][*it].c;
		  words->push_back(word);
		  word.clear();
		  sum += countPoints(2, begin, end, line, b);
		}
	    }
	    break;
	  case 2://w wierszu                                                                                                                                      
	    {
	      begin = goLeft((*it),line-1,b.tmp_board);
	      end = goRight((*it),line+1, b.tmp_board);
	      if (begin < end)
		{
		  for(int p = begin; p<=end; p++)
		    word+=b.tmp_board[*it][p].c;
		  words->push_back(word);
		  word.clear();
		  sum += countPoints(2, begin, end, line, b);
		}
	    }
	    break;
	  }
      }
  }
  *points = sum;
}

int goUp(int i, int j, letter board[ROW_NUM][COL_NUM]) //idz do góry dopoki nie skonczą się litery lub plansza                                                                                     
    {
      int a;
      for (a = i; a>=0; a--)
	{
	  if(board[a][j].c == "")
	    break;
	}
  return a+1;
}

int goDown(int i, int j, letter board[ROW_NUM][COL_NUM]) //idz w dół dopoki nie skonczą się litery lub plansza                                                                         
{
  int a;
  for (a = i; a<ROW_NUM; a++)
    {
      if(board[a][j].c == "")
        break;
    }
  return a-1;
}
int goLeft(int i, int j, letter board[ROW_NUM][COL_NUM]) //idz w lewo dopoki nie skonczą się litery lub plansza                                                                                    
{
  int a;
  for (a = j; a>=0; a--)
    {
      if(board[i][a].c == "")
        break;
    }

  return a+1;
}

int goRight(int i, int j, letter board[ROW_NUM][COL_NUM]) //idz w prawo dopoki nie skonczą się litery lub plansza                                                                                  
{
  int a;
  for (a = j; a < COL_NUM; a++)
    {
      if(board[i][a].c == "")
        break;
    }

  return a-1;
}

int countPoints(int option, int begin, int end, int x, board b)
{
  int word_multiplier = 1;//ustaw początkowo mnożnik słowa na 1                                                                                                        

  int sum = 0;

  switch(option)
    {
    case 1://jeśli wyraz w wierszu                                                                                                                                      
      for(int j = begin; j <= end; j++) {
	if(b.matrix[x][j] == b.tmp_board[x][j].val)
	  sum+=2*b.tmp_board[x][j].val;
	else if (b.matrix[x][j] == 6) word_multiplier*= 2;
	else sum+=b.tmp_board[x][j].val;
      }
       //policz dla każdej komórki, sprawdz czy nie zmienił sie mnożnik słowa                                    
      break;

    case 2:
      for(int j = begin; j <= end; j++) {
	if(b.matrix[j][x] == b.tmp_board[j][x].val)
	  sum+=2*b.tmp_board[j][x].val;
	else if (b.matrix[j][x] == 6) word_multiplier*=2;
	else sum+=b.tmp_board[j][x].val;
      }
       break;
    }
  sum*=word_multiplier;

  return sum; 
}

void clearBoards(board *b) {
  for(int i = 0; i < ROW_NUM; i++)
    for(int j = 0 ; j < COL_NUM; j++) {
      b->tmp_board[i][j].c = "";
      b->tmp_board[i][j].val = 0;
      b->let_board[i][j].c="";
      b->let_board[i][j].val = 0;
    }
}
