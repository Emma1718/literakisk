all:serwer
serwer: board.o dict.o letters.o serwer.o 
	  g++ -g -pthread board.o dict.o letters.o serwer.o  -o serwer 
board.o: board.cpp board.h letters.h
	    g++ -Wall -c board.cpp -o board.o 
dict.o: dict.cpp dict.h
	    g++ -Wall -c dict.cpp -o dict.o 
letters.o: letters.cpp letters.h
	    g++ -Wall -c letters.cpp -o letters.o 
serwer.o: serwer.cpp board.h letters.h dict.h
	    g++ -Wall -c serwer.cpp -o serwer.o 
clean:
	      rm *.o
	      rm serwer
