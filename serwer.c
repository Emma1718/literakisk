#include <sys/socket.h>
#include <netinet/in.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <strings.h>
#include <unistd.h>
#include <pthread.h>
#include <string.h>

#define CON_LIMIT 2

ushort service_port = 12345;

pthread_mutex_t sock_mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_t client_threads[CON_LIMIT];
pthread_t main_thread;
int cln_sck[CON_LIMIT];
int limit = 0;
char players[2][30];

void* client_loop(void* arg)
{
  char buffer[1024];
  int sck = *((int*) arg);
  int rcv;
  int i = 0;

  printf("Hi I'm socket %d\n", sck);
  
  while((rcv=read(sck, buffer, 1024))>0)//(rcv = recv(sck, buffer, 1024, 0))>0)
    {
      if (buffer[0] == 'S')
	{
	  char * name = strtok(buffer, "|");
	  name = strtok(NULL, "|");
	  printf("Hi, %s\n", name);
	  for (i = 0; i < CON_LIMIT; i++)
	    if (cln_sck [i] == sck) {
	      strcpy(players[i], name);
	      write(cln_sck[1-i], name, strlen(name)+1);
	    }
	}
    }

  pthread_mutex_lock(&sock_mutex);
  for (i = 0; i < CON_LIMIT; i++)
    if (cln_sck [i] == sck) {
      cln_sck [i] = 0;//cln_sck[1];
      if(cln_sck[1-i] != 0)
	write(cln_sck[1-i], "DIS", 4);
    }
  pthread_mutex_unlock(&sock_mutex);
  printf("Socket %d is getting closed...\n", sck);
  close(sck);
}


void* main_loop(void* arg)
{
  struct sockaddr_in server_addr, cln_addr;

  int sck, tmp_sck;

  int i = CON_LIMIT;

  bzero(&server_addr, sizeof(server_addr));
  server_addr.sin_addr.s_addr = INADDR_ANY;
  server_addr.sin_family = AF_INET;
  server_addr.sin_port = htons(service_port);
  int x;
  for(x = 0;x<CON_LIMIT;x++)
    {
      cln_sck[x] = 0;
    }

  if((sck = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0) 
    {
      perror("Nie można utworzyć gniazdka");
      exit(EXIT_FAILURE);
    }

  int opt=1;

  // lose the pesky "Address already in use" error message
  if (setsockopt(sck,SOL_SOCKET,SO_REUSEADDR,&opt,sizeof(int)) == -1) {
    perror("setsockopt");
    exit(1);
  }  
 

  if(bind(sck, (struct sockaddr*) &server_addr, sizeof server_addr) < 0)
    {
      printf("Cannot bind socket %d in %d port \n", sck, service_port);
      exit(EXIT_FAILURE);
    }

  if(listen(sck,-1) < 0) 
    {
      perror("Cannot listen"); 
      exit(EXIT_FAILURE);
    }

  printf("Main loop\n");
  while(1)
    {
     
      socklen_t cln_addr_size = sizeof cln_addr;
  
      if((tmp_sck = accept(sck, (struct sockaddr*) &cln_addr, (socklen_t*) &cln_addr_size)) <0)
	{
	  perror("Main socket accept error\n");
	  exit(EXIT_FAILURE);
	}
      
      
      pthread_mutex_lock(&sock_mutex);
      i = CON_LIMIT;
      int j;
      for(j = 0; j<CON_LIMIT; j++)
	{
	  if(cln_sck[j] == 0)
	    {
	      i = j;
	      cln_sck[j] = tmp_sck;
	      printf("Accepting socket...\n");
	      if(cln_sck[1-i] == 0) {
		printf("First player, waiting for opponent...");
		char * buffer =  "S|wait"; 
		write(tmp_sck,buffer, strlen(buffer)+1);
	      } else {
		printf("Opponent is connected! Game is starting...");
		char * buf1 =  "S|false";
		write(tmp_sck,buf1,strlen(buf1)+1);
		char * buf2 =  "S|true";
		write(cln_sck[1-i],buf2,strlen(buf2)+1); 
	      }	   
	      break;	    
	    }
	}
      
      pthread_mutex_unlock(&sock_mutex);
  

      if(i == CON_LIMIT)
	{
	  printf("Too many connections!\n");
	  close(tmp_sck);
	  continue;
	}

      if(pthread_create(&client_threads[i], NULL, client_loop, &cln_sck[i]) !=0)
	{
	  printf("Error creating new client thread");
	  continue;
	}
    }	
    
}

int main( int argc, char** argv)
{
  if (pthread_create(&main_thread, NULL, main_loop, NULL) != 0) {
    printf("ERROR");

  }
  printf("Start\n");
  getchar();

  return 0;
}
