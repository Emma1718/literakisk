#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <iostream>

using namespace std;
MainWindow::MainWindow(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->main->setVisible(false);
    ui->IPEdit->setInputMask("000.000.000.000;_");
    ui->portEdit->setInputMask("00000;_");
    ui->progressBar->setFormat("Rozlaczono");

    flags[0] = false;
    flags[1] = false;
    flags[2] = false;


    tmp_letter = "";
    tmp_color = QColor(255,255,255);
}

MainWindow::~MainWindow()
{
    delete ui;
}
void MainWindow::timerEnd(){
    if (tmove == 0)
    {

        if(!tmp_letter.isEmpty()) {
            for(int k = 0; k < 7; k++) {
                if(ui->playerLetters->item(0,k)->text() == "") {
                    ui->playerLetters->item(0,k)->setText(tmp_letter);
                    ui->playerLetters->item(0,k)->setBackgroundColor(tmp_color);
                    ui->tmpLetter->item(0,0)->setText("");
                    ui->tmpLetter->item(0,0)->setBackgroundColor(QColor(255,255,255));
                    break;
                }
            }
        }
        setEnabledForAll(false);

        deleteModLetters();
        map->deleteMods();

        socket->write("C00#", 3);
        ui->log->appendPlainText("Koniec czasu! Tracisz kolejkę!");
        ui->label_7->setText(QString::number(tmove));
        timer->stop();

    }
    else
    {
        ui->label_7->setText(QString::number(tmove));
        tmove--;

    }
}
//komentarze tylko do testów
void MainWindow::on_connectBtn_clicked()
{
    timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()), this, SLOT(timerEnd()));
    if(ui->nickEdit->text().length() > 0 && ui->IPEdit->text().length() > 0 && ui->portEdit->text().length() > 0 ) {
        QHostInfo::lookupHost(ui->IPEdit->text(), this, SLOT(hostLookedUp(QHostInfo))); //

        tmove = 60;
    }
    // timer->start(1000);
}

void MainWindow::hostLookedUp(const QHostInfo &info)
{
    if(info.error() != QHostInfo::NoError) {
        QMessageBox::critical(this, tr("Błąd!"), "Podano niepoprawne dane.");
        return ;
    }
    bool ok = true;
    quint16 portNumber = ui->portEdit->text().toInt(&ok, 10);

    if(ok) {
        socket = new QTcpSocket(this);
        connect(socket, SIGNAL(error(QAbstractSocket::SocketError)), this, SLOT(socketError(QAbstractSocket::SocketError)));
        connect(socket, SIGNAL(connected()), this, SLOT(socketConnected()));
        connect(socket, SIGNAL(disconnected()), this, SLOT(socketDisconnected()));
        connect(socket, SIGNAL(readyRead()), this, SLOT(readAnswer()));
        socket->connectToHost(info.hostName(), (quint16)portNumber);
        // ui->log->appendPlainText("Łączenie z " + info.hostName() + "...");
        for(int i = 0; i <=100; i++) {
            ui->progressBar->setValue(i);
            if(i < 100) ui->progressBar->setFormat("Laczenie z " +  info.hostName());
        }
    }
}

void MainWindow::socketError(QAbstractSocket::SocketError err)
{
    //if (err==QAbstractSocket::RemoteHostClosedError) {
    for(int i = 100; i >=0; i-=2)
        ui->progressBar->setValue(i);
    ui->progressBar->setFormat("Rozlaczono");
    ui->main->setVisible(false);
    ui->connectBtn->setEnabled(true);
    ui->nickEdit->setReadOnly(false);
    //}
    // cout<< "Socket error: " << socket->errorString()<< endl;
    return;

    delete socket;
}

void MainWindow::socketConnected()
{

    ui->progressBar->setFormat("Polaczono");
    ui->main->setVisible(true);
    ui->connectBtn->setEnabled(false);
    ui->nickEdit->setReadOnly(true);
    ui->IPEdit->setReadOnly(true);
    ui->portEdit->setReadOnly(true);

    QString nick = ui->nickEdit->text();
    myName = nick;

}

void MainWindow::socketDisconnected()
{
    for(int i = 100; i >=0; i-=2)
        ui->progressBar->setValue(i);
    ui->progressBar->setFormat("Rozlaczono");
    ui->main->setVisible(false);
    ui->connectBtn->setEnabled(true);
    ui->nickEdit->setReadOnly(false);
    ui->log->clear();
    deleteModLetters();
    map->deleteMods();
    ui->board->clear();
    ui->playerLetters->clear();
    for(int i = 0 ; i < 5; i++)
        flags[i] = false;

    gameStart = false;
}

void MainWindow::readBoard(char* bufor) {
    //ui->log->appendPlainText(bufor);
    for(int i = 0; i < ROW_COUNT; i++)
        for(int j = 0; j < COL_COUNT; j++) {
            matrix[i][j] = bufor[(j%COL_COUNT)+(COL_COUNT*i)]-'0';
            //ui->log->appendPlainText(QChar::fromAscii(matrix[i][j]));
            //scout << matrix[i][j];
        }
}

void MainWindow::readAnswer()
{
    QByteArray ba = socket->readAll();



    // ui->log->appendPlainText(ba.data());
    QList<QByteArray> msgs = ba.split('#');


    while(!msgs.isEmpty()) {
        QByteArray msg = msgs.takeFirst();
        // ui->log->appendPlainText(msg.data());
        QList<QByteArray> data1 = msg.split('|');
        QByteArray mainMsg = data1.last();
        // ui->log->appendPlainText(mainMsg.data());

        char c = msg[0];
        switch(c) {
        case 'S': {
            if(strcmp(mainMsg.data(), "true") == 0) {
                myTurn = true;
                ui->log->appendPlainText("Gra rozpoczeta! Twoj ruch");
                QString nick = myName;
                nick.prepend("S|");
                QByteArray ba = nick.toAscii();
                char * nickMsg = ba.data();
                socket->write(nickMsg, strlen(nickMsg)+1);
                flags[0] = true;
                // ui->log->appendPlainText("Flaga 0 ustawiona");

            } else if(strcmp(mainMsg.data(), "wait") == 0) {
                ui->player1Name->setText(myName);
                ui->player2Name->setText("");
                ui->log->appendPlainText("Oczekiwanie na przeciwnika...");
            } else if(strcmp(mainMsg.data(), "false") == 0){
                if(ui->player1Name->text().isEmpty())
                    ui->player1Name->setText(myName);
                else ui->player2Name->setText(myName);
                ui->log->appendPlainText("Gra rozpoczeta! Zaczyna Twoj przeciwnik");
                QString nick = myName;
                nick.prepend("S|");
                QByteArray ba = nick.toAscii();
                char * nickMsg = ba.data();
                socket->write(nickMsg, strlen(nickMsg)+1);
                flags[0] = true;
                //ui->log->appendPlainText("Flaga 0 ustawiona");

            }
        }
            break;
        case 'G': {

            showPlayerLetters(mainMsg.data());

        } break;
        case 'O': {
            if(ui->player1Name->text() != myName)
                ui->player1Name->setText(mainMsg.data());
            else ui->player2Name->setText(mainMsg.data());
            QString logInfo = "Twoj przeciwnik to ";
            logInfo.append(mainMsg.data());
            ui->log->appendPlainText(logInfo);
            flags[1] = true;
        }
            break;

        case 'D': {
            ui->log->appendPlainText("Twój przeciwnik rozłączył się");
            endOfGame();
            ui->player2Name->setText("");
            ui->pntsPl1->setText("0");
            ui->pntsPl2->setText("0");
            setEnabledForAll(false);
            gameStart = false;
            flags[0] = false;
            flags[1] = false;
        }    break;
        case 'B': {
            readBoard(mainMsg.data());
            flags[2] = true;

        } break;
        case 'L': {
            loadValsOfLetters(mainMsg.data());
        }
            break;
        case 'C': {
            if(strcmp(mainMsg.data(), "correct") == 0) {
                ui->log->appendPlainText("Słowa zostały zatwierdzone przez serwer.");
                map->setModifiedFields();
            } else {
                ui->log->appendPlainText("Słowa zostały odrzucone przez serwer.");
                deleteModLetters();
            }
            map->deleteMods();
            setEnabledForAll(false);
            myTurn = false;
            timer->stop();

        } break;
        case 'P': {
            // ui->log->appendPlainText(mainMsg.data());
            QList<QByteArray> points = mainMsg.split('&');
            ui->pntsPl1->setText(points[0]);
            ui->pntsPl2->setText(points[1]);

        } break;
        case 'M': {
            //ui->log->appendPlainText(mainMsg.data());
            if(strcmp(mainMsg.data(), "0") == 0)
                ui->log->appendPlainText("Twój przeciwnik pasuje. Twoj ruch");
            else {
                ui->log->appendPlainText("Twoja kolejka");
                setLetters(mainMsg);
            }
            setEnabledForAll(true);
            myTurn = true;
        } break;
        case 'E': {
            timer->stop();
            if(strcmp(mainMsg.data(), "letters") == 0) {
                sendLeftLetters();
                ui->log->appendPlainText("Obaj gracze stracili po 2 kolejki z rzędu. Następuje odejmowanie punktów za pozostałe liery...");
            }
            else if(strcmp(mainMsg.data(), "winner") == 0)
                ui->log->appendPlainText("Wygrałeś! Gratulacje!");
            else if(strcmp(mainMsg.data(), "looser") == 0)
                ui->log->appendPlainText("Wygrał Twój przeciwnik");
            else ui->log->appendPlainText("Remis");

            setEnabledForAll(false);
        } break;
        }
    }

    if(flags[0] && flags[1] && flags[2]) {
        if(!gameStart) {
            new_game();
            socket->write("G|7#", 4);
            gameStart = true;
            // ui->log->appendPlainText("Wszystkie flagi ustawione");
        }
        if(myTurn){
            if(!timer->isActive()) {
                timer->start(1000);
                tmove = 60;
            }
            setEnabledForAll(true);
        }
        else {
            setEnabledForAll(false);
        }

    }


}
void MainWindow::sendLeftLetters() {
    QString msg = "E|";
    for(int i = 0; i < 7; i++) {
        if(!ui->playerLetters->item(0,i)->text().isEmpty()){
            msg += ui->playerLetters->item(0,i)->text() + ".";
        }
    }
    msg+="#";
    char * toSend = new char[msg.length()];
    toSend = msg.toUtf8().data();
    socket->write(toSend, strlen(toSend));
}

void MainWindow::deleteModLetters() {
    for(int i = 0; i < ROW_COUNT; i++)
        for(int j = 0; j < COL_COUNT; j++) {
            if(map->checkIfModified(i,j)) {
                QString delLetter = ui->board->item(i,j)->text();
                QColor delColor = ui->board->item(i,j)->backgroundColor();
                // ui->log->appendPlainText("Del: " + delLetter);
                for(int k = 0; k < 7; k++) {
                    if(ui->playerLetters->item(0,k)->text() == "") {
                        ui->playerLetters->item(0,k)->setText(delLetter);
                        ui->playerLetters->item(0,k)->setBackgroundColor(delColor);
                        break;
                    }
                }
                ui->board->item(i,j)->setText("");
                colorItem(i,j,matrix[i][j]);


            }
        }
}

void MainWindow::setLetters(QByteArray coords) {
    QList<QByteArray> letters = coords.split('!');
    // ui->log->appendPlainText(coords.data());

    for(int i = 0 ; i < letters.size(); i++) {
        QByteArray let = letters[i];
        // ui->log->appendPlainText(let.data());

        if(strcmp(let.data(), "") != 0) {
            QList<QByteArray> parts = let.split('.');
            QString letter = parts[0];
            bool ok;
            int row = parts[1].toInt(&ok, 10);
            //ui->log->appendPlainText(row);

            int col = parts[2].toInt(&ok, 10);
            ui->board->item(row,col)->setText(letter);
            colorItem(row, col,valsOfLetters.value(letter));
            map->setField(row,col);

        }

    }
}

void MainWindow::setEnabledForAll(bool x) {
    ui->okBtn->setEnabled(x);
    ui->playerLetters->setEnabled(x);
    ui->board->setEnabled(x);
    ui->passBtn->setEnabled(x);
}

void MainWindow::endOfGame() {

    ui->board->clear();
    ui->playerLetters->clear();
    ui->okBtn->setEnabled(false);
    ui->passBtn->setEnabled(false);
    disconnect(ui->board, SIGNAL(cellClicked(int, int)), this, SLOT(myBoardCellClicked(int, int)));
    disconnect(ui->playerLetters, SIGNAL(cellClicked(int, int)), this, SLOT(myLettersCellClicked(int, int)));
}


void MainWindow::loadValsOfLetters(char * letters){
    QString characters = QString::fromUtf8(letters);

    for(int i = 0; i < characters.size(); i+=2) {
        valsOfLetters.insert(QString(characters[i]), characters[i+1].digitValue());
        //   ui->log->appendPlainText((QChar) characters[i]);
    }


}
void MainWindow::new_game() {

    for(int i = 0; i < ROW_COUNT; i++)
        for(int j = 0; j < COL_COUNT; j++) {
            QTableWidgetItem *item = new QTableWidgetItem();

            item->setTextAlignment(Qt::AlignCenter);
            item->setFont(QFont("Sans", 10, QFont::Bold));
            item->setFlags(Qt::ItemIsSelectable);
            item->setText("");
            item->setBackgroundColor(QColor(255,255,255));
            ui->board->setItem(j,i,item);
        }

    for(int i = 0; i < 7; i++) {
        QTableWidgetItem *item = new QTableWidgetItem();

        item->setTextAlignment(Qt::AlignCenter);
        item->setFont(QFont("Sans", 10, QFont::Bold));
        item->setFlags(Qt::NoItemFlags);
        item->setText("");
        item->setBackgroundColor(QColor(255,255,255));

        ui->playerLetters->setItem(0,i,item);

    }

    QTableWidgetItem *tmp = new QTableWidgetItem();
    tmp->setTextAlignment(Qt::AlignCenter);
    tmp->setFont(QFont("Sans", 10, QFont::Bold));
    tmp->setFlags(Qt::NoItemFlags);
    tmp->setText("");
    tmp->setBackgroundColor(QColor(255,255,255));
    ui->tmpLetter->setItem(0,0,tmp);

    connect(ui->board, SIGNAL(cellClicked(int, int)), this, SLOT(myBoardCellClicked(int, int)));
    connect(ui->playerLetters, SIGNAL(cellClicked(int, int)), this, SLOT(myLettersCellClicked(int, int)));

    for(int i = 0; i < 15; i++)
        for(int j = 0; j < 15; j++)
            colorItem(i,j, matrix[i][j]);
    map = new Map();

}

void MainWindow::colorItem(int i, int j, int col) {

    switch(col) {
    case 0: ui->board->item(i,j)->setBackgroundColor(QColor(255,255,255));
        break;
    case 1: ui->board->item(i,j)->setBackgroundColor(QColor(255,255,0));
        break;
    case 2: ui->board->item(i,j)->setBackgroundColor(QColor(0,255,0));
        break;

    case 3: ui->board->item(i,j)->setBackgroundColor(QColor(0,255,255));
        break;
    case 5: ui->board->item(i,j)->setBackgroundColor(QColor(255,0,0));
        break;
    case 6:
        ui->board->item(i,j)->setBackgroundColor(QColor(128,128,128));
        break;

    }
}

void MainWindow::showPlayerLetters(char * letters) {

    QString characters = QString::fromUtf8(letters);


    for(int i = 0; i < characters.size(); i++) {
        for( int k = 0; k < 7; k++) {
            if(ui->playerLetters->item(0,k)->text().isEmpty()) {
                ui->playerLetters->item(0,k)->setText(QString(characters[i]));

                switch(valsOfLetters.value(QString(characters[i]))) {
                case 1: ui->playerLetters->item(0,k)->setBackgroundColor(QColor(255,255,0));
                    break;
                case 2: ui->playerLetters->item(0,k)->setBackgroundColor(QColor(0,255,0));
                    break;
                case 3: ui->playerLetters->item(0,k)->setBackgroundColor(QColor(0,255,255));
                    break;
                case 5: ui->playerLetters->item(0,k)->setBackgroundColor(QColor(255,0,0));
                    break;


                }
                break;
            }
        }
    }
}

void MainWindow::myBoardCellClicked(int i, int j) {

    if(map->checkIfSet(i,j) == false) {
        QString tmp = ui->board->item(i,j)->text();
        QColor  tmp_c = ui->board->item(i,j)->backgroundColor();

        if(tmp_letter.isEmpty()) {
            map->modify(i,j,false);
            colorItem(i,j, matrix[i][j]); //powrót do starego koloru
        }
        else {
            map->modify(i,j,true);
            ui->board->item(i,j)->setBackgroundColor(tmp_color);
        }

        if(tmp.length() > 0) {
            tmp_color = tmp_c;
            // ui->log->appendPlainText(tmp);
        } else tmp_color = QColor(255,255,255);

        ui->board->item(i,j)->setText(tmp_letter);
        tmp_letter = tmp;

        //ui->log->appendPlainText(tmp_letter);
        ui->tmpLetter->item(0,0)->setBackgroundColor(tmp_color);
        ui->tmpLetter->item(0,0)->setText(tmp_letter);
    }

}

void MainWindow::myLettersCellClicked(int i, int j) {

    QString tmp = ui->playerLetters->item(i,j)->text();
    QColor  tmp_c = ui->playerLetters->item(i,j)->backgroundColor();


    //ui->log->appendPlainText(tmp);
    ui->playerLetters->item(i,j)->setText(tmp_letter);
    ui->playerLetters->item(i,j)->setBackgroundColor(tmp_color);
    tmp_letter = tmp;
    tmp_color = tmp_c;

    //ui->log->appendPlainText(tmp_letter);

    ui->tmpLetter->item(0,0)->setBackgroundColor(tmp_color);
    ui->tmpLetter->item(0,0)->setText(tmp_letter);

}

void MainWindow::on_okBtn_clicked() {

    int cs, n_mods;
    bool testMove = map->checkMove(cs, n_mods);
    QString messageTmp = "C";
    timer->stop();
    if (testMove)
    {
        ui->log->appendPlainText("Poprawny ruch ! ");
        messageTmp+=QString::number(cs)+QString::number(n_mods)+"|"; //przesyłana też informacja o tym czy główny wyraz znajduje się w kolumnie czy rzędzie, liczba modyfikacji

        for(int i = 0; i < ROW_COUNT; i++)
            for(int j = 0; j < COL_COUNT; j++)
                if (map->checkIfModified(i,j))
                {
                    if(ui->board->item(i,j)->text() == "_") {
                        ui->board->item(i,j)->setText(ui->freeLetter->itemText(ui->freeLetter->currentIndex()));
                    }
                    messageTmp=messageTmp+ui->board->item(i,j)->text()+"."+QString::number(i)+"."+QString::number(j)+"!";
                    //     ui->log->appendPlainText(messageTmp);
                }
        messageTmp=messageTmp+"#";

        //  ui->log->appendPlainText("COORDS: " + messageTmp);
        char * message = new char[(messageTmp.length()+1)];
        message = messageTmp.toUtf8().data();
        socket->write(message, strlen(message)+1);
    } else {
        socket->write("C00", 3);
        ui->log->appendPlainText("Niepoprawny ruch! Strata kolejki");
        setEnabledForAll(false);
    }
    if(!tmp_letter.isEmpty()) {
        for(int k = 0; k < 7; k++) {
            if(ui->playerLetters->item(0,k)->text() == "") {
                ui->playerLetters->item(0,k)->setText(tmp_letter);
                ui->playerLetters->item(0,k)->setBackgroundColor(tmp_color);
                ui->tmpLetter->item(0,0)->setText("");
                ui->tmpLetter->item(0,0)->setBackgroundColor(QColor(255,255,255));

                break;
            }
        }
    }
}

void MainWindow::on_passBtn_clicked() {
    ui->log->appendPlainText("Pas - Strata kolejki");
    socket->write("C00", 3);
    setEnabledForAll(false);

    if(!tmp_letter.isEmpty()) {
        for(int k = 0; k < 7; k++) {
            if(ui->playerLetters->item(0,k)->text() == "") {
                ui->playerLetters->item(0,k)->setText(tmp_letter);
                ui->playerLetters->item(0,k)->setBackgroundColor(tmp_color);
                ui->tmpLetter->item(0,0)->setText("");
                ui->tmpLetter->item(0,0)->setBackgroundColor(QColor(255,255,255));
                break;
            }
        }
    }
}
